#include "Shape.h"

// constructor
Shape::Shape(const std::string& name, const std::string& type) : _name(name), _type(type)
{
}
// default destructor
Shape::~Shape()
{
}

// function prints info of shape
void Shape::printDetails() const
{
	std::cout << "Name: " << this->_name << std::endl;
	std::cout << "Type: " << this->_type << std::endl;
	std::cout << "Area: " << this->getArea() << std::endl;
	std::cout << "Peri: " << this->getPerimeter() << std::endl;
}

// getters
std::string Shape::getType() const
{
	return this->_type;
}
std::string Shape::getName() const
{
	return this->_name;
}

// function sets color of shape
void Shape::setColor(const Color color)
{
	this->_color = color;
}

// function moves all points in vector by offset
void Shape::movePoints(std::vector<Point> points, Point offset)
{
	int i = 0;
	// for all points
	for (i = 0; i < points.size(); i++) {
		points[i] += offset;
	}
}

