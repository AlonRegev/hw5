#include "Menu.h"

Menu::Menu() : _canvas()
{
}

Menu::~Menu()
{
	this->deleteAllShapes();
}

int Menu::chooseOption()
{
	int choice = 0;
	std::cout << "What do you want to do?\n";
	std::cout << "0. add a new shape\n";
	std::cout << "1. get / change shape info\n";
	std::cout << "2. delete all shapes\n";
	std::cout << "3. exit\n";
	std::cout << "Your choice: ";
	
	std::cin >> choice;
	
	switch (choice) {
	case ADD:
		addShape();
		break;
	case INFO:
		shapeInfo();
		break;
	case DELETE_ALL:
		break;
		deleteAllShapes();
	case QUIT:
		break;
	default:
		std::cout << "\nInvalid input!\n";
		break;
	}

	return choice;
}

// function gets new shape from user to add
void Menu::addShape()
{
	int choice = 0;
	Shape* newShape = nullptr;
	std::cout << "0. Circle\n1. Arrow\n2. Triangle\n3. Rectangle\n4. Quadrangle\nEnter shape to add: ";
	std::cin >> choice;

	// create new shape and try to add it to list
	switch (choice) {
	case CIRCLE:
		newShape = new Circle(Point::input(), Menu::getSize("Enter radius: "), "circle", Menu::getName(), Menu::getColor());
		this->addToList(newShape);
		break;
	case ARROW:
		newShape = new Arrow(Point::input(), Point::input(), "arrow", Menu::getName(), Menu::getColor());
		this->addToList(newShape);
		break;
	case TRIANGLE:
		newShape = new Triangle(Point::input(), Point::input(), Point::input(), "triangle", Menu::getName(), Menu::getColor());
		this->addToList(newShape);
		break;
	case RECTANGLE:
		newShape = new myShapes::Rectangle(Point::input(), Menu::getSize("Enter length: "), Menu::getSize("Enter width: "), "rectangle", Menu::getName(), Menu::getColor());
		this->addToList(newShape);
		break;
	case QUADRANGLE:
		std::cout << "Consecutive points are adjacent. invalid: intersects itself, 3+ points on 1 line.\n";
		newShape = new Quadrangle(Point::input(), Point::input(), Point::input(), Point::input(), "quadrangle", Menu::getName(), Menu::getColor());
		this->addToList(newShape);
		break;
	default:
		std::cout << "Invalid Input!\n";
	}
}

//function adds shape to list of shapes
void Menu::addToList(Shape* newShape)
{
	if (newShape->isValid()) {
		// add shape
		this->_shapeList.push_back(newShape);
		newShape->draw(this->_canvas);
	}
	else {
		// delete shape
		delete newShape;
		std::cout << "Invalid shape...";
	}
}

// function edits or displays shape's info
void Menu::shapeInfo()
{
	int shapeIndex = 0;
	int option = 0;
	int i = 0;
	if (this->_shapeList.size() > 0) {
		// print list of shapes
		for (i = 0; i < this->_shapeList.size(); i++) {
			std::cout << "Enter " << i << " for " << this->_shapeList[i]->getName() << "(" << this->_shapeList[i]->getType() << ")\n";
		}

		std::cout << "Which shape? ";
		std::cin >> shapeIndex;

		// options
		std::cout << "0. move shape\n1. get details\n2. remove shape\n3. change color\nChoose option: ";
		std::cin >> option;
		switch (option) {
		case MOVE:
			this->_shapeList[shapeIndex]->clearDraw(this->_canvas);
			this->_shapeList[shapeIndex]->move(Point::input());
			this->redraw();
			break;
		case DETAILES:
			this->_shapeList[shapeIndex]->printDetails();
			break;
		case REMOVE:
			this->_shapeList[shapeIndex]->clearDraw(this->_canvas);
			delete this->_shapeList[shapeIndex];
			this->_shapeList.erase(this->_shapeList.begin() + shapeIndex);
			this->redraw();
			break;
		case COLOR:
			this->_shapeList[shapeIndex]->setColor(Menu::getColor());
			this->redraw();		// we have to redraw everything.
			break;
		default:
			std::cout << "Invalid Input!\n";
			break;
		}
	}
	else {
		std::cout << "No shapes yet.\n";
	}
}

// function deletes all shapes
void Menu::deleteAllShapes()
{
	// free memory of all shapes and clear draw
	while (this->_shapeList.size() > 0) {
		this->_shapeList.back()->clearDraw(this->_canvas);
		delete this->_shapeList.back();
		this->_shapeList.pop_back();
	}
}

void Menu::redraw()
{
	int i = 0;
	// redraw all shapes
	for (i = 0; i < this->_shapeList.size(); i++) {
		this->_shapeList[i]->draw(this->_canvas);
	}
}

// function gets a geometric size, meaning return value > 0
double Menu::getSize(const std::string msg)
{
	double value = 0;
	// get value
	std::cout << msg;
	std::cin >> value;
	while (value <= 0) {
		std::cout << "Invlid Input! Try again: ";
		std::cin >> value;
	}

	return value;
}

// function gets color from user
Color Menu::getColor()
{
	int choice = 0;
	Color color = Color::white;
	std::cout << "Enter color 0 (red) 1 (green) 2 (blue) 3 (white - default) 4 black: ";
	std::cin >> choice;
	// didn't find better solution... int -> color
	switch (choice) {
	case 0:
		color = Color::red;
		break;
	case 1:
		color = Color::green;
		break;
	case 2:
		color = Color::blue;
		break;
	case 3:
		color = Color::white;
		break;
	case 4:
		color = Color::black;
		break;
	default:
		std::cout << "Invalid input. using white.\n";
		break;
	}

	return color;
}

// function gets shapes name from user
std::string Menu::getName()
{
	std::string name = "";
	bool nameValid = false;
	int i = 0;
	// try to get name
	do
	{
		std::cout << "Enter name: ";
		std::cin >> name;
		// search for name in existing shapes
		nameValid = true;
		for (i = 0; i < this->_shapeList.size(); i++) {
			if (this->_shapeList[i]->getName() == name) {
				nameValid = false;
				std::cout << "Name already exists. ";
			}
		}
	} while (!nameValid);

	return name;
}

