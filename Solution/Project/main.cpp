#include "Menu.h"

void test() {
	Menu menu;
	while (menu.chooseOption() != QUIT) {
		// choose option until user chooses quit, then end program
	}
}

void main()
{
	test();		// in another function to check memory leaks!

	if (_CrtDumpMemoryLeaks())
	{
		std::cout << "\nMemory leaks!\n";
	}
	else
	{
		std::cout << "\nNo leaks\n";
	}
}