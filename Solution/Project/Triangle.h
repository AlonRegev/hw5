#pragma once
#include "Polygon.h"
#include <string>

class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name, const Color color);
	virtual ~Triangle();

	virtual void draw(const Canvas& canvas);
	virtual void clearDraw(const Canvas& canvas);
	
	virtual double getArea() const;
	virtual double getPerimeter() const;

	virtual bool isValid();
};