#include "Rectangle.h"

// canvas functions
void myShapes::Rectangle::draw(const Canvas& canvas)
{
	canvas.draw_rectangle(_points[0], _points[1], this->_color);
}
void myShapes::Rectangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_rectangle(_points[0], _points[1]);
}

// get shape info
double myShapes::Rectangle::getArea() const
{
	return this->_length * this->_width;
}
double myShapes::Rectangle::getPerimeter() const
{
	return 2 * (this->_length + this->_width);
}

// function checks if shape is valid
bool myShapes::Rectangle::isValid()
{
	return this->_length > 0 && this->_width > 0;
}

// ctor, dtor
myShapes::Rectangle::Rectangle(const Point& a, double length, double width, const std::string& type, const std::string& name, const Color color) : Polygon(type, name)
{
	// add 2 points to list
	Point offset(width, length);
	this->_points.push_back(a);
	this->_points.push_back(a + offset);

	this->_length = length;
	this->_width = width;

	this->_color = color;
}
myShapes::Rectangle::~Rectangle()
{

}
