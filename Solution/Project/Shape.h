#pragma once
#include "Point.h"
#include "Canvas.h"
#include <string>
#include <iostream>
#include <vector>

class Shape 
{
public:
	Shape(const std::string& name, const std::string& type);
	virtual ~Shape();
	virtual double getArea() const = 0;
	virtual double getPerimeter() const = 0;
	virtual void draw(const Canvas& canvas) = 0;
	virtual void clearDraw(const Canvas& canvas) = 0;
	virtual void move(const Point& other) = 0; // add the Point to all the points of shape
	virtual bool isValid() = 0;
	void printDetails() const;
	std::string getType() const;
	std::string getName() const;
	void setColor(const Color color);

protected:
	static void movePoints(std::vector<Point> points, Point offset);
	Color _color;

private:
	std::string _name;
	std::string _type;
};