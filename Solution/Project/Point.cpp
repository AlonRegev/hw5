#include "Point.h"

// constructors
Point::Point(double x, double y) : _x(x), _y(y)
{
}
Point::Point(const Point& other) : _x(other._x), _y(other._y)
{
}
// default destructor
Point::~Point()
{
}

// operator overloading
Point Point::operator+(const Point& other) const
{
	return Point(this->_x + other._x, this->_y + other._y);
}
Point& Point::operator+=(const Point& other)
{
	this->_x += other._x;
	this->_y += other._y;
	return *this;
}

// getters
double Point::getX() const
{
	return this->_x;
}
double Point::getY() const
{
	return this->_y;
}

// function calculates distance between 2 points
double Point::distance(const Point& other) const
{
	return std::sqrt(std::pow(this->_x - other._x, 2) + std::pow(this->_y - other._y, 2));
}

// function gets a point input from user
Point Point::input()
{
	int x = 0, y = 0;
	std::cout << "Enter X: ";
	std::cin >> x;
	std::cout << "Enter Y: ";
	std::cin >> y;
	return Point(x, y);
}
