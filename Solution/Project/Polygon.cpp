#include "Polygon.h"

Polygon::Polygon(const std::string& type, const std::string& name) : Shape(name, type)
{
}

// function moves shape by an offset presented with a point
void Polygon::move(const Point& other)
{
	Shape::movePoints(this->_points, other);
}
