#pragma once
#include "Polygon.h"

class Arrow : public Shape
{
public:
	Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name, const Color color);
	~Arrow();

	virtual void draw(const Canvas& canvas);
	virtual void clearDraw(const Canvas& canvas);

	virtual double getArea() const;
	virtual double getPerimeter() const;

	virtual void move(const Point& other);

	virtual bool isValid();

private:
	std::vector<Point> _points;
};