#pragma once
#include "Canvas.h"
#include "Rectangle.h"
#include "Triangle.h"
#include "Circle.h"
#include "Arrow.h"
#include "Quadrangle.h"
#include <vector>

enum option {ADD, INFO, DELETE_ALL, QUIT};
enum shapes { CIRCLE, ARROW, TRIANGLE, RECTANGLE, QUADRANGLE };
enum infoOptions { MOVE, DETAILES, REMOVE, COLOR };

class Menu
{
public:

	Menu();
	~Menu();

	int chooseOption();

	void addShape();
	void addToList(Shape* newShape);
	void shapeInfo();
	void deleteAllShapes();
	void redraw();

	static double getSize(const std::string msg);
	static Color getColor();
	std::string getName();

private: 
	Canvas _canvas;
	std::vector<Shape*> _shapeList;
};

