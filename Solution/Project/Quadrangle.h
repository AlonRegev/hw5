#pragma once
#include "Triangle.h"

#define VERTICAL_M 1000000000

class Quadrangle : public Polygon
{
public:
	Quadrangle(const Point& a, const Point& b, const Point& c, const Point& d, const std::string& type, const std::string& name, const Color color);
	virtual ~Quadrangle();

	virtual void draw(const Canvas& canvas);
	virtual void clearDraw(const Canvas& canvas);

	virtual double getArea() const;
	virtual double getPerimeter() const;

	virtual bool isValid();

	virtual void move(Point& other);

private:
	Triangle* _triangle1;
	Triangle* _triangle2;
	double _perimeter;

	// geometric tools
	static bool areIntersecting(Point a1, Point a2, Point b1, Point b2);
	static bool whichSideOfLine(Point a1, Point a2, Point p);
};

