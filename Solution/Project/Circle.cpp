#include "Circle.h"

// ctor, dtor
Circle::Circle(const Point& center, double radius, const std::string& type, const std::string& name, const Color color) : Shape(name, type), _center(center), _radius(radius)
{
	this->_color = color;
}
Circle::~Circle()
{
}

// getters
const Point& Circle::getCenter() const
{
	return this->_center;
}
double Circle::getRadius() const
{
	return this->_radius;
}

// canvas functions
void Circle::draw(const Canvas& canvas)
{
	canvas.draw_circle(getCenter(), getRadius(), this->_color);
}
void Circle::clearDraw(const Canvas& canvas)
{
	canvas.clear_circle(getCenter(), getRadius());
}

// get shape info
double Circle::getArea() const
{
	return PI * this->_radius * this->_radius;
}
double Circle::getPerimeter() const
{
	return 2 * PI * this->_radius;
}

// function moves shape by an offset presented by a point
void Circle::move(const Point& other)
{
	this->_center += other;
}

// function checks if shape is valid
bool Circle::isValid()
{
	return this->_radius > 0;
}

