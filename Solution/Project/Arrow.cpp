#include "Arrow.h"

// ctor, dtor
Arrow::Arrow(const Point& a, const Point& b, const std::string& type, const std::string& name, const Color color) : Shape(name, type)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_color = color;
}
Arrow::~Arrow()
{
}

// canvas functions
void Arrow::draw(const Canvas& canvas)
{
	canvas.draw_arrow(_points[0], _points[1], this->_color);
}
void Arrow::clearDraw(const Canvas& canvas)
{
	canvas.clear_arrow(_points[0], _points[1]);
}

// get shape info
double Arrow::getArea() const
{
	return 0;
}
double Arrow::getPerimeter() const
{
	return this->_points[0].distance(this->_points[1]);
}

// function moves shape by an offset presented with a point
void Arrow::move(const Point& other)
{
	Shape::movePoints(this->_points, other);
}

// function checks if shape is valid
bool Arrow::isValid()
{
	return this->_points[0].distance(this->_points[1]) != 0;	// same point
}
