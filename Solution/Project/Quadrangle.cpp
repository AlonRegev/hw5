#include "Quadrangle.h"

// ctor, dtor
Quadrangle::Quadrangle(const Point& a, const Point& b, const Point& c, const Point& d, const std::string& type, const std::string& name, const Color color) : Polygon(type, name)
{
	// assuming quad is valid (no intersecting edges)
	// which diagonal to use to cut quad to triangles? Checking BD
	// check if diagonal (BD) is outside shape
	if (!areIntersecting(a, c, b, d)){	// diagonals not intersecting => one is outside shape
		if (whichSideOfLine(b, d, a) == whichSideOfLine(b, d, c)) {		// a and c are on the same side of BD (below or above) => BD is out of the shape
			// diagonal AC, triangle BAC, DAC
			this->_triangle1 = new Triangle(b, a, c, "", "", color);
			this->_triangle2 = new Triangle(d, a, c, "", "", color);
		}
		else {
			// BD is the inside diagonal, triangle ABD, CBD
			this->_triangle1 = new Triangle(a, b, d, "", "", color);
			this->_triangle2 = new Triangle(c, b, d, "", "", color);
		}
	}
	else {
		// default BD, triangle ABD, CBD
		this->_triangle1 = new Triangle(a, b, d, "", "", color);
		this->_triangle2 = new Triangle(c, b, d, "", "", color);
	}

	// points
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_points.push_back(c);
	this->_points.push_back(d);
}
Quadrangle::~Quadrangle()
{
	delete this->_triangle1;
	delete this->_triangle2;
}

// draw 2 triangles
void Quadrangle::draw(const Canvas& canvas)
{
	this->_triangle1->draw(canvas);
	this->_triangle2->draw(canvas);
}
// clear 2 triangles
void Quadrangle::clearDraw(const Canvas& canvas)
{
	this->_triangle1->clearDraw(canvas);
	this->_triangle2->clearDraw(canvas);
}
// get sum of areas of 2 triangles
double Quadrangle::getArea() const
{
	return this->_triangle1->getArea() + this->_triangle2->getArea();
}
// get perimeter
double Quadrangle::getPerimeter() const
{
	// sum of sides = distances between points
	return this->_points[0].distance(this->_points[1]) + this->_points[1].distance(this->_points[2]) + this->_points[2].distance(this->_points[3]) + this->_points[3].distance(this->_points[0]);;
}

// checks if shape is valid
bool Quadrangle::isValid()
{
	// check that no area of triangles is 0 (3 points on the same line and quad is actually a triangle or worse, a line)
	// check that opposite sides are not intersecting
	return !areIntersecting(this->_points[0], this->_points[1], this->_points[2], this->_points[3]) && // AB, CD
		!areIntersecting(this->_points[1], this->_points[2], this->_points[0], this->_points[3]) && // BC, AD
		!_triangle1->getArea() == 0 && !_triangle2->getArea() == 0;
}

// function moves shape by offset other
void Quadrangle::move(Point& other)
{
	Polygon::move(other);
	this->_triangle1->move(other);
	this->_triangle2->move(other);
}

// checks if lines a and b are intersecting
bool Quadrangle::areIntersecting(Point a1, Point a2, Point b1, Point b2)
{
	double mA = 0, mB = 0, bA = 0, bB = 0;
	double intersectionX;
	double aMinX = std::min(a1.getX(), a2.getX());
	double aMaxX = std::max(a1.getX(), a2.getX());
	double bMinX = std::min(b1.getX(), b2.getX());
	double bMaxX = std::max(b1.getX(), b2.getX());

	// find intersection
	// find m, b
	// if x1 = x2 , using estimation: m = 1 billion. huge, but not close to max value of double

	// finding line a ~~~~~~~~~~~~~~~~~~
	if (a1.getX() == a2.getX()) {
		mA = VERTICAL_M;
	}
	else {
		// m = dy / dx
		mA = (a1.getY() - a2.getY()) / (a1.getX() - a2.getX());
	}
	// b = y - mx
	bA = a1.getY() - mA * a1.getX();
	// finding line b ~~~~~~~~~~~~~~~~~~
	if (b1.getX() == b2.getX()) {
		mB = VERTICAL_M;
	}
	else {
		// m = dy / dx
		mB = (b1.getY() - b2.getY()) / (b1.getX() - b2.getX());
	}
	// b = y - mx
	bA = a1.getY() - mA * a1.getX();

	// finding intersection
	// x = (b2 - b1) / (m1 - m2)
	if (mA != mB) {	// parallel, no intersection
		intersectionX = (bB - bA) / (mA - mB);
	}
	return mA != mB &&	// not parallel
		(aMinX <= intersectionX && intersectionX <= aMaxX &&
		bMinX <= intersectionX && intersectionX <= bMaxX);
}

// function checks if point p is above or below line a
bool Quadrangle::whichSideOfLine(Point a1, Point a2, Point p)
{
	double m = 0, b = 0;

	if (a1.getX() == a2.getX()) {	// vertical
		return p.getX() > a1.getX();
	}
	else {
		// m = dy / dx
		m = (a1.getY() - a2.getY()) / (a1.getX() - a2.getX());
		// b = y - mx
		b = a1.getY() - m * a1.getX();

		return m * p.getX() + b < p.getY();		// at p.x, is p below or above the line?
	}
}

