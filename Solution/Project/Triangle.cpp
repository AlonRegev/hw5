#include "Triangle.h"

// canvas functions
void Triangle::draw(const Canvas& canvas)
{
	canvas.draw_triangle(_points[0], _points[1], _points[2], this->_color);
}
void Triangle::clearDraw(const Canvas& canvas)
{
	canvas.clear_triangle(_points[0], _points[1], _points[2]);
}

// get shape info
double Triangle::getArea() const
{
	double res = 0;
	// using formual of area of a triangle by 3 points
	// S = |ax(by - cy) + bx(cy - ay) + cx(ay - by)| / 2
	// a = [0], b = [1], c = [2]
	res += this->_points[0].getX() * (this->_points[1].getY() - this->_points[2].getY());
	res += this->_points[1].getX() * (this->_points[2].getY() - this->_points[0].getY());
	res += this->_points[2].getX() * (this->_points[0].getY() - this->_points[1].getY());
	return std::abs(res / 2);
}
double Triangle::getPerimeter() const
{
	// sum of the three sides
	return this->_points[0].distance(this->_points[1]) + this->_points[1].distance(this->_points[2]) + this->_points[2].distance(this->_points[0]);
}

// function checks if shape is valid
bool Triangle::isValid()
{
	return this->getArea() != 0;	// if area is 0, points are on the same line
}

// ctor, dtor
Triangle::Triangle(const Point& a, const Point& b, const Point& c, const std::string& type, const std::string& name, const Color color) : Polygon(type, name)
{
	this->_points.push_back(a);
	this->_points.push_back(b);
	this->_points.push_back(c);

	this->_color = color;
}
Triangle::~Triangle()
{
}
